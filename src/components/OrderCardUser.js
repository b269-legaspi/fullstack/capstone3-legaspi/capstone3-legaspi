import Loader from "../components/Loader";

import { Link, useNavigate } from "react-router-dom";

import { Button, Row, Card, ListGroup } from "react-bootstrap";

import Swal from "sweetalert2";

export default function OrderCardUser({ isLoading, order }) {
  const { _id, products, totalAmount, purchasedOn } = order;
  const navigate = useNavigate();

  const cancelOrderFunction = (e) => {
    e.preventDefault();
    Swal.fire({
      title: "Do you want to cancel this order?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#3085d6",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      reverseButtons: true,
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`${process.env.REACT_APP_API_URL}/orders/${_id}/cancel`, {
          method: "PATCH",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
          .then((res) => res.json())
          .then((data) => {
            console.log(data);

            if (data) {
              Swal.fire(
                "Order succssfully cancelled",
                "You have cancelled an order",
                "success"
              ).then(function () {
                navigate(0);
              });
            } else {
              Swal.fire("Something went wrong", "Please, try again.", "error");
            }
          });
      }
    });

    // added confirmation message in swal
    // e.preventDefault();
    // fetch(`${process.env.REACT_APP_API_URL}/orders/${_id}/cancel`, {
    //   method: "PATCH",
    //   headers: {
    //     "Content-Type": "application/json",
    //     Authorization: `Bearer ${localStorage.getItem("token")}`,
    //   },
    // })
    //   .then((res) => res.json())
    //   .then((data) => {
    //     console.log(data);
    //     if (data) {
    //       Swal.fire({
    //         title: "Order successful",
    //         icon: "success",
    //         text: "You have placed an order",
    //       });
    //       navigate("/products");
    //     } else {
    //       Swal.fire({
    //         title: "Something went wrong",
    //         icon: "error",
    //         text: "Please, try again.",
    //       });
    //     }
    //   });
  };

  return isLoading ? (
    <Loader />
  ) : (
    <Row className="m-1">
      <Card className="cardHighlight p-0">
        <Card.Body>
          <div className="text-center">
            <Card.Subtitle>Order Id </Card.Subtitle>
            <Card.Text>{_id}</Card.Text>
            <Card.Subtitle>Products</Card.Subtitle>
          </div>
          {products.map((product) => (
            <>
              <ListGroup variant="flush">
                <ListGroup.Item>
                  <h6 className="text-center">{product.name}</h6>
                  <img
                    src={product.imgLink}
                    class="card-img-top p-3"
                    alt="Card"
                  />
                </ListGroup.Item>
                <ListGroup.Item>Product Id: {product.productId}</ListGroup.Item>
                <ListGroup.Item>
                  Description: {product.description}
                </ListGroup.Item>
                <ListGroup.Item>Price: {product.price}</ListGroup.Item>
                <ListGroup.Item>Quantity: {product.quantity}</ListGroup.Item>
                <ListGroup.Item>
                  <p>Subtotal: {product.subTotal}</p>
                  <div className="text-center">
                    <Button
                      className="bg-primary"
                      as={Link}
                      to={`/products/${product.productId}`}
                    >
                      View Product
                    </Button>
                  </div>
                </ListGroup.Item>
                <ListGroup.Item></ListGroup.Item>
              </ListGroup>
            </>
          ))}
          <div className="text-center">
            <Card.Subtitle>Total Amount</Card.Subtitle>
            <Card.Text>{totalAmount}</Card.Text>
            <Card.Subtitle>Purchased On</Card.Subtitle>
            <Card.Text>{purchasedOn}</Card.Text>

            <Button
              className="bg-danger"
              type="button"
              onClick={cancelOrderFunction}
            >
              Cancel Order
            </Button>
          </div>
        </Card.Body>
      </Card>
    </Row>
  );
}
