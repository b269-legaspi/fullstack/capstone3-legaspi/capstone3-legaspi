import Loader from "../components/Loader";

import { Link, useNavigate } from "react-router-dom";

import { Button, Card } from "react-bootstrap";

import Swal from "sweetalert2";

export default function CartCard({ isLoading, cart }) {
  const { productId, name, description, price, quantity, subTotal, imgLink } =
    cart;

  const navigate = useNavigate();

  const removeToCartFunction = (e) => {
    e.preventDefault();
    Swal.fire({
      title: "Do you want to remove this product?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#3085d6",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      reverseButtons: true,
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`${process.env.REACT_APP_API_URL}/carts/add-to-cart`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
          body: JSON.stringify({ productId: productId, quantity: 0 }),
        })
          .then((res) => res.json())
          .then((data) => {
            console.log(data);

            if (data) {
              Swal.fire("Product removed to cart", "", "success").then(
                function () {
                  navigate(0);
                }
              );
            } else {
              Swal.fire("Something went wrong", "Please, try again.", "error");
            }
          });
      }
    });
  };

  return isLoading ? (
    <Loader />
  ) : (
    <Card className="cardHighlight p-0">
      <Card.Body>
        <Card.Title>
          <h4 className="text-center">{name}</h4>
        </Card.Title>
        <img src={imgLink} class="card-img-top p-3" alt="Card" />
        <Card.Subtitle>Product ID</Card.Subtitle>
        <Card.Text>{productId}</Card.Text>
        <Card.Subtitle>Name</Card.Subtitle>
        <Card.Text>{name}</Card.Text>
        <Card.Subtitle>Description</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Card.Subtitle>Quantity</Card.Subtitle>
        <Card.Text>{quantity}</Card.Text>
        <Card.Subtitle>Subtotal</Card.Subtitle>
        <Card.Text>{subTotal}</Card.Text>
        <div className="text-center">
          <Button
            className="mx-2"
            variant="primary"
            type="button"
            as={Link}
            to={`/products/${productId}`}
          >
            Edit Quantity
          </Button>
          <Button
            className="mx-2"
            variant="danger"
            type="button"
            onClick={removeToCartFunction}
            // onClick={orderProduct}
            // onClick={}
          >
            Remove
          </Button>
        </div>
      </Card.Body>
    </Card>
  );
}

// const removeToCartFunctionOld = (e) => {
//   e.preventDefault();

//   fetch(`${process.env.REACT_APP_API_URL}/carts/add-to-cart`, {
//     method: "POST",
//     headers: {
//       "Content-Type": "application/json",
//       Authorization: `Bearer ${localStorage.getItem("token")}`,
//     },
//     body: JSON.stringify({ productId: productId, quantity: 0 }),
//   })
//     .then((res) => res.json())
//     .then((data) => {
//       console.log(data);

//       if (data) {
//         navigate(0);
//       } else {
//         Swal.fire({
//           title: "Something went wrong",
//           icon: "error",
//           text: "Please, try again.",
//         });
//       }
//     });
// };
