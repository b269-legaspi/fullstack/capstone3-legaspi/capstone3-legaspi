import Loader from "../components/Loader";

import { Button, Card } from "react-bootstrap";

export default function ProductCardAdmin({ isLoading, product }) {
  const { _id, imgLink, name, description, price } = product;

  console.log(isLoading);

  return isLoading ? (
    <Loader />
  ) : (
    <Card className="cardHighlight p-0">
      <Card.Body>
        <Card.Title>
          <h4 className="text-center">{name}</h4>
        </Card.Title>
        <img src={imgLink} class="card-img-top p-3" alt="Card" />
        <Card.Subtitle>Product ID</Card.Subtitle>
        <Card.Text>{_id}</Card.Text>
        <Card.Subtitle>Name</Card.Subtitle>
        <Card.Text>{name}</Card.Text>
        <Card.Subtitle>Description</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <div className="text-center">
          <Button className="bg-primary" disabled>
            Product Details
          </Button>
        </div>
      </Card.Body>
    </Card>
  );
}
