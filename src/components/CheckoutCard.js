import { useNavigate } from "react-router-dom";

import { Button, Row, Col, Card } from "react-bootstrap";

import Swal from "sweetalert2";

export default function CheckoutCard({ data }) {
  const { totalAmount } = data;

  const navigate = useNavigate();

  const checkOutCartFunction = (e) => {
    e.preventDefault();

    e.preventDefault();
    Swal.fire({
      title: "Continue checkout?",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      // confirmButtonColor: "#d33",
      // cancelButtonColor: "#3085d6",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      reverseButtons: true,
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`${process.env.REACT_APP_API_URL}/carts/checkout-cart`, {
          method: "PATCH",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
          .then((res) => res.json())
          .then((data) => {
            console.log(data);
            if (data) {
              Swal.fire({
                title: "Order successful",
                icon: "success",
                text: "You have placed an order",
              }).then(function () {
                navigate("/orders");
              });
            } else {
              Swal.fire({
                title: "Something went wrong",
                icon: "error",
                text: "Please, try again.",
              });
            }
          });
      }
    });
  };

  return (
    <Row>
      <Col lg={4} className="mx-auto my-2 text-center">
        <Card className="cardHighlight p-0">
          <Card.Body>
            <Card.Subtitle>Total Amount</Card.Subtitle>
            <Card.Text>{totalAmount}</Card.Text>
            <Button
              variant="primary"
              type="button"
              onClick={checkOutCartFunction}
            >
              Check Out Orders
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
