import { Link } from "react-router-dom";

import { Button, Row, Card, ListGroup } from "react-bootstrap";

export default function OrderCardAdmin({ order }) {
  const { _id, user, products, totalAmount, purchasedOn } = order;

  return (
    <Row className="m-1">
      <Card className="cardHighlight p-0">
        <Card.Body>
          <Card.Subtitle>User Id</Card.Subtitle>
          <Card.Text>{user?.userId}</Card.Text>
          <Card.Subtitle>Email</Card.Subtitle>
          <Card.Text>{user?.email}</Card.Text>
          <Card.Subtitle>Order Id </Card.Subtitle>
          <Card.Text>{_id}</Card.Text>
          <Card.Subtitle>Products</Card.Subtitle>
          {products.map((product) => (
            <>
              <ListGroup variant="flush">
                <ListGroup.Item>
                  <h6>{product.name}</h6>
                  <img
                    src={product.imgLink}
                    class="card-img-top p-3"
                    alt="Card"
                  />
                </ListGroup.Item>
                <ListGroup.Item>Product Id: {product.productId}</ListGroup.Item>
                <ListGroup.Item>
                  Description: {product.description}
                </ListGroup.Item>
                <ListGroup.Item>Price: {product.price}</ListGroup.Item>
                <ListGroup.Item>Quantity: {product.quantity}</ListGroup.Item>
                <ListGroup.Item>
                  <p>Subtotal: {product.subTotal}</p>
                  <Button
                    className="bg-primary"
                    as={Link}
                    to={`/products-admin-card/${product.productId}`}
                  >
                    View Product
                  </Button>
                </ListGroup.Item>
                <ListGroup.Item></ListGroup.Item>
              </ListGroup>
            </>
          ))}
          <Card.Subtitle>Total Amount</Card.Subtitle>
          <Card.Text>{totalAmount}</Card.Text>
          <Card.Subtitle>Purchased On</Card.Subtitle>
          <Card.Text>{purchasedOn}</Card.Text>
          <Button
            className="bg-primary"
            as={Link}
            to={`/orders-card/${user?.userId}`}
          >
            See all orders by this user
          </Button>
        </Card.Body>
      </Card>
    </Row>
  );
}
