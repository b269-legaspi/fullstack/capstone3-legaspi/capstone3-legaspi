import { useState, useEffect } from "react";

import AppNavbar from "./components/AppNavbar";

import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import ChangePass from "./pages/ChangePass";
import Error from "./pages/Error";
import CreateProduct from "./pages/CreateProduct";
import UpdateProduct from "./pages/UpdateProduct";
import DeactivateProduct from "./pages/DeactivateProduct";
import AdminDashboardPage from "./pages/AdminDashboardPage";
import GetActiveProducts from "./pages/GetActiveProducts";
import UserSingleProduct from "./pages/UserSingleProduct";
import UserDetails from "./pages/UserDetails";
import Checkout from "./pages/Checkout";
import UserGetActiveOrders from "./pages/UserGetActiveOrders";
import AdminGetActiveOrders from "./pages/AdminGetActiveOrders";
import GetAdminProducts from "./pages/GetAdminProducts";
import AdminGetActiveOrdersTableView from "./pages/AdminGetActiveOrdersTableView";
import AdminGetUserSpecificOrders from "./pages/AdminGetUserSpecificOrders";
import AdminSingleProduct from "./pages/AdminSingleProduct";
import AdminGetUsersSpecificOrdersTableView from "./pages/AdminGetUsersSpecificOrdersTableView";
import AdminGetAllUsers from "./pages/AdminGetAllUsers";

import { Container } from "react-bootstrap";

import { UserProvider } from "./UserContext";

import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import "./App.css";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        } else {
          setUser({
            id: null,
            isAdmin: null,
          });
        }
      });
  }, []);
  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <AppNavbar />
          <Container>
            {user.isAdmin ? (
              <Routes>
                <Route path="/" element={<AdminDashboardPage />} />
                <Route path="/register" element={<Register />} />
                <Route path="/login" element={<Login />} />
                <Route path="/change-pass" element={<ChangePass />} />
                <Route path="/logout" element={<Logout />} />
                <Route
                  path="/admin-dashboard"
                  element={<AdminDashboardPage />}
                />
                <Route path="/products-admin" element={<GetAdminProducts />} />
                <Route path="/products-create" element={<CreateProduct />} />
                <Route
                  path="/products-update/:productId"
                  element={<UpdateProduct />}
                />
                <Route
                  path="/products-deactivate/:productId"
                  element={<DeactivateProduct />}
                />
                <Route
                  path="/orders"
                  element={<AdminGetActiveOrdersTableView />}
                />
                <Route
                  path="/orders/:userId"
                  element={<AdminGetUsersSpecificOrdersTableView />}
                />
                <Route path="/orders-card" element={<AdminGetActiveOrders />} />
                <Route
                  path="/orders-card/:userId"
                  element={<AdminGetUserSpecificOrders />}
                />
                <Route
                  path="/products-admin-card/:productId"
                  element={<AdminSingleProduct />}
                />
                <Route path="/users" element={<AdminGetAllUsers />} />

                <Route path="/*" element={<Error />} />
              </Routes>
            ) : user.id !== null ? (
              <Routes>
                <Route path="/" element={<GetActiveProducts />} />
                <Route path="/register" element={<Register />} />
                <Route path="/login" element={<Login />} />
                <Route path="/change-pass" element={<ChangePass />} />
                <Route path="/logout" element={<Logout />} />
                <Route path="/products" element={<GetActiveProducts />} />
                <Route
                  path="/products/:productId"
                  element={<UserSingleProduct />}
                />
                <Route path="/user-details" element={<UserDetails />} />
                <Route path="/checkout" element={<Checkout />} />
                <Route path="/orders" element={<UserGetActiveOrders />} />
                <Route path="/*" element={<Error />} />
              </Routes>
            ) : (
              <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/register" element={<Register />} />
                <Route path="/login" element={<Login />} />
                <Route path="/logout" element={<Logout />} />
                <Route path="/products" element={<GetActiveProducts />} />
                <Route
                  path="/products/:productId"
                  element={<UserSingleProduct />}
                />
                <Route path="/*" element={<Error />} />
              </Routes>
            )}
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
