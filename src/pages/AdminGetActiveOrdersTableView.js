import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";

export default function AdminGetActiveOrdersTableView() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/all-orders`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setOrders(
          data.map((order) => {
            // return <OrderCardAdmin key={order._id} order={order} />;
            return (
              <tr key={order._id}>
                <td className="px-3">{order._id}</td>
                <td className="px-3">{order.user?.email}</td>
                <td className="px-3">{order.user?.userId}</td>
                <td>
                  {" "}
                  <Button
                    className="bg-primary"
                    as={Link}
                    to={`/orders/${order.user?.userId}`}
                  >
                    View
                  </Button>
                </td>
                <td className="px-3">{order.totalAmount}</td>
                <td className="px-3">{order.purchasedOn}</td>
              </tr>
            );
          })
        );
      });
  }, []);

  return (
    <>
      <Button className="bg-primary mt-2" as={Link} to={`/orders-card`}>
        Card View
      </Button>
      <div className="table-responsive">
        <table className="mt-3">
          <thead>
            <tr>
              <th>Order ID</th>
              <th>Email</th>
              <th>User ID</th>
              <th>User's Orders</th>
              <th>Total Amount</th>
              <th>Purchased on</th>
            </tr>
          </thead>
          <tbody>{orders}</tbody>
        </table>
      </div>
    </>
  );
}
