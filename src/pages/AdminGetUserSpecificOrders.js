import { useState, useEffect } from "react";
import { Button, Row, Col } from "react-bootstrap";

import { Link, useParams } from "react-router-dom";

import OrderCardAdmin from "../components/OrderCardAdmin";

export default function AdminGetUserSpecificOrders() {
  const [orders, setOrders] = useState([]);

  const { userId } = useParams();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/all-orders/${userId}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setOrders(
          data.map((order) => {
            return (
              <>
                <Col md={6} lg={4} className="mx-auto my-2">
                  <OrderCardAdmin key={order._id} order={order} />;
                </Col>
              </>
            );
          })
        );
      });
  }, [userId]);

  return (
    <>
      <Button className="bg-primary mt-2" as={Link} to={`/orders/${userId}`}>
        User's Orders - Table View
      </Button>
      <Button className="bg-primary mt-2" as={Link} to={`/orders-card`}>
        View All Users' Orders
      </Button>
      <Row>{orders}</Row>
    </>
  );
}
