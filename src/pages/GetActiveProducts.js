import { useState, useEffect } from "react";

import { Row, Col } from "react-bootstrap";

import ProductCardUser from "../components/ProductCardUser";

export default function GetActiveProducts() {
  const [products, setProducts] = useState([]);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setLoading(false); //hide Loader.js when data is fetched
        setProducts(
          data.map((product) => {
            return (
              <>
                <Col md={6} lg={4} className="mx-auto my-2">
                  <ProductCardUser
                    key={product._id}
                    isLoading={isLoading}
                    product={product}
                  />
                </Col>
              </>
            );
          })
        );
      });
  }, [isLoading]);

  return (
    <>
      <Row>{products}</Row>
    </>
  );
}
