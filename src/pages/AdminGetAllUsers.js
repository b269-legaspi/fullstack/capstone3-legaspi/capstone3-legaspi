import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Button } from "react-bootstrap";

import Swal from "sweetalert2";

export default function AdminGetAllUsers() {
  const [users, setUsers] = useState([]);
  const navigate = useNavigate();

  const promoteUserToAdminFunction = (userId) => {
    Swal.fire({
      title: "Do you want to promote this user?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      reverseButtons: true,
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(
          `${process.env.REACT_APP_API_URL}/users/${userId}/set-user-to-admin`,
          {
            method: "PATCH",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        )
          .then((res) => res.json())
          .then((data) => {
            console.log(data);

            if (data) {
              Swal.fire("User successfully promoted", "", "success").then(
                function () {
                  navigate(0);
                }
              );
            } else {
              Swal.fire("Something went wrong", "Please, try again.", "error");
            }
          });
      }
    });
  };

  const demoteAdminToUserFunction = (userId) => {
    Swal.fire({
      title: "Do you want to DEMOTE this ADMIN?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#3085d6",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      reverseButtons: true,
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(
          `${process.env.REACT_APP_API_URL}/users/${userId}/demote-admin-to-user`,
          {
            method: "PATCH",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        )
          .then((res) => res.json())
          .then((data) => {
            console.log(data);

            if (data) {
              Swal.fire("Admin succssfully demoted", "", "success").then(
                function () {
                  navigate(0);
                }
              );
            } else {
              Swal.fire("Something went wrong", "Please, try again.", "error");
            }
          });
      }
    });
  };

  useEffect(
    () => {
      fetch(`${process.env.REACT_APP_API_URL}/users/all-users`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          setUsers(
            data.map((user) => {
              return (
                <tr key={user._id}>
                  <td className="px-3">{user.email}</td>
                  <td className="px-3">{user._id}</td>
                  <td className="px-3">{user.isAdmin.toString()}</td>
                  <td className="px-3">
                    {user.isAdmin ? (
                      <Button
                        className="table-btn btn-danger"
                        onClick={() => demoteAdminToUserFunction(user._id)}
                      >
                        Demote
                      </Button>
                    ) : (
                      <Button
                        className="table-btn btn-primary"
                        onClick={() => promoteUserToAdminFunction(user._id)}
                      >
                        Promote
                      </Button>
                    )}
                  </td>
                </tr>
              );
            })
          );
        });
    }
    // ,[]
  );

  return (
    <>
      <div className="table-responsive">
        <table className="mt-3">
          <thead>
            <tr>
              <th className="px-3">Email</th>
              <th className="px-3">User ID</th>
              <th className="px-3">is Admin?</th>
              <th className="px-3">Controls</th>
            </tr>
          </thead>
          <tbody>{users}</tbody>
        </table>
      </div>
    </>
  );
}
