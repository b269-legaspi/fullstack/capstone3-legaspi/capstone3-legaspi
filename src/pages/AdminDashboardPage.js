import { useState, useEffect } from "react";

import { Link, useNavigate } from "react-router-dom";
import { Button } from "react-bootstrap";

import Swal from "sweetalert2";

// import ProductCardAdmin from "../components/ProductCardAdmin";

export default function AdminDashboardPage() {
  const navigate = useNavigate();
  const [products, setProducts] = useState([]);

  const archiveProductFunction = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: "Updated status.",
            icon: "success",
            timer: 1500,
            timerProgressBar: true,
            didOpen: () => {
              Swal.showLoading();
            },
          }).then(function () {
            navigate(0);
          });
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please, try again.",
          });
        }
      });
  };

  const reactivateProductFunction = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/reactivate`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: "Updated status.",
            icon: "success",
            timer: 1500,
            timerProgressBar: true,
            didOpen: () => {
              Swal.showLoading();
            },
          }).then(function () {
            navigate(0);
          });
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please, try again.",
          });
        }
      });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProducts(
          data.map((product) => {
            // return <ProductCardAdmin key={product._id} product={product} />;
            const event = new Date(product.createdOn);
            console.log(event.toString());
            return (
              <tr key={product._id}>
                <td className="px-3">
                  <Button
                    className="bg-primary"
                    as={Link}
                    to={`/products-update/${product._id}`}
                  >
                    Update
                  </Button>
                  {product.isActive ? (
                    <Button
                      className="table-btn btn-danger"
                      onClick={() => archiveProductFunction(product._id)}
                    >
                      Archive
                    </Button>
                  ) : (
                    <Button
                      className="table-btn btn-primary"
                      onClick={() => reactivateProductFunction(product._id)}
                    >
                      Show
                    </Button>
                  )}
                </td>
                <td className="px-3">{product.name}</td>
                <td className="px-3">{product._id}</td>
                <td className="px-3">{product.description}</td>
                <td className="px-3">{product.price}</td>
                <td className="px-3">{product.isActive.toString()}</td>
                <td className="px-3">{event.toString()}</td>
                <td className="px-3">
                  <Button
                    className="table-btn bg-primary "
                    href={product.imgLink}
                    target="_blank"
                  >
                    View
                  </Button>
                </td>
              </tr>
            );
          })
        );
      });
  }, []);

  return (
    <>
      <Button
        className="bg-primary mt-2  mx-2"
        as={Link}
        to={`/products-create`}
      >
        Create New Product
      </Button>
      <Button className="bg-primary mt-2 mx-2" as={Link} to={`/products-admin`}>
        View Product Cards
      </Button>
      <div className="table-responsive">
        <table className="mt-3">
          <thead>
            <tr>
              <th>Controls</th>
              <th>Name</th>
              <th>Product Id</th>
              <th>Description</th>
              <th>Price</th>
              <th>Is Active?</th>
              <th>Created On</th>
              <th>Image Link</th>
            </tr>
          </thead>
          <tbody>{products}</tbody>
        </table>
      </div>
    </>
  );
}
