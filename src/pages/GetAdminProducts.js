import ProductCardAdmin from "../components/ProductCardAdmin";

import { useState, useEffect } from "react";

import { Row, Col } from "react-bootstrap";

export default function GetAdminProducts() {
  const [products, setProducts] = useState([]);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    // fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setLoading(false); //hide Loader.js when data is fetched
        setProducts(
          data.map((product) => {
            return (
              <Col md={6} lg={4} className="mx-auto my-2">
                <ProductCardAdmin
                  key={product._id}
                  isLoading={isLoading}
                  product={product}
                />
              </Col>
            );
          })
        );
      });
  }, [isLoading]);

  return (
    <>
      <Row>{products}</Row>
    </>
  );
}

// old codes -- GetAllProducts.js
// import { useState, useEffect } from "react";

// import ProductCardAdmin from "../components/ProductCardAdmin";

// export default function GetAllProducts() {
//   const [products, setProducts] = useState([]);

//   useEffect(() => {
//     fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
//       method: "GET",
//       headers: {
//         "Content-Type": "application/json",
//         Authorization: `Bearer ${localStorage.getItem("token")}`,
//       },
//     })
//       .then((res) => res.json())
//       .then((data) => {
//         console.log(data);
//         setProducts(
//           data.map((product) => {
//             return <ProductCardAdmin key={product._id} product={product} />;
//           })
//         );
//       });
//   }, []);

//   return <>{products}</>;
// }
