import { useState, useEffect, useContext } from "react";

import UserContext from "../UserContext";

import { Navigate, useNavigate } from "react-router-dom";

import Swal from "sweetalert2";

import { Form, Button } from "react-bootstrap";

export default function ChangePassword() {
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const [oldPassword, setOldPassword] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (
      oldPassword !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [oldPassword, password1, password2]);

  function changePassFunction(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/change-pass`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        currentPassword: oldPassword,
        newPassword: password1,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.value === "Current password is incorrect") {
          Swal.fire({
            title: "Current password is incorrect",
            icon: "error",
            text: "Please try again",
          });
        } else if (data.value === "Please provide a new password") {
          Swal.fire({
            title: "Please provide a new password",
            icon: "error",
            text: "Please try again",
          });
        } else if (data) {
          setOldPassword("");
          setPassword1("");
          setPassword2("");

          Swal.fire({
            title: "Successfully Changed Password",
            icon: "success",
            text: "Log in again",
          });
          navigate("/logout");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please, try again.",
          });
        }
      });
  }

  return user.id == null ? (
    <Navigate to="/products" />
  ) : (
    <Form onSubmit={(e) => changePassFunction(e)}>
      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Enter your current password</Form.Label>
        <Form.Control
          type="password"
          value={oldPassword}
          onChange={(e) => {
            setOldPassword(e.target.value);
          }}
          placeholder="Enter email"
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>New Password</Form.Label>
        <Form.Control
          type="password"
          value={password1}
          onChange={(e) => {
            setPassword1(e.target.value);
          }}
          placeholder="Enter Your Password"
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Verify New Password</Form.Label>
        <Form.Control
          type="password"
          value={password2}
          onChange={(e) => {
            setPassword2(e.target.value);
          }}
          placeholder="Verify Your Password"
        />
      </Form.Group>
      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
