import { useState, useEffect, useContext } from "react";

import { useParams, Link, useNavigate } from "react-router-dom";

import { Button, Row, Col, Card, Form } from "react-bootstrap";

import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function UserSingleProduct() {
  const { user, setUser } = useContext(UserContext);

  const { productId } = useParams();

  const [products, setProducts] = useState([]);
  const [quantity, setQuantity] = useState("");

  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/carts/user-cart`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // test purposes
        // const myArray = [
        //   { id: "73", foo: "bar" },
        //   { id: "45", foo: "bar" },
        // ];
        // console.log(myArray.find((x) => x.id === "45").foo);
        // console.log(data);
        // console.log(data[0].cartProducts);
        // console.log(
        //   data[0].cartProducts.find(
        //     (products) => products.productId === productId
        //   ).quantity
        // );
        // test purposes
        setQuantity(
          data[0].cartProducts.find(
            (products) => products.productId === productId
          ).quantity
        );
      })
      .catch(() => setQuantity(""));
  }, [productId]);

  const navigate = useNavigate();

  //  replaced by addToCartFunction feature
  // const orderProduct = (e) => {
  //   e.preventDefault();

  //   fetch(`${process.env.REACT_APP_API_URL}/orders/order-product`, {
  //     method: "POST",
  //     headers: {
  //       "Content-Type": "application/json",
  //       Authorization: `Bearer ${localStorage.getItem("token")}`,
  //     },
  //     body: JSON.stringify([{ productId: productId, quantity: quantity }]),
  //   })
  //     .then((res) => res.json())
  //     .then((data) => {
  //       console.log(data);

  //       if (data) {
  //         Swal.fire({
  //           title: "Order successful",
  //           icon: "success",
  //           text: "You have placed an order",
  //         });

  //         navigate("/products");
  //       } else {
  //         Swal.fire({
  //           title: "Something went wrong",
  //           icon: "error",
  //           text: "Please, try again.",
  //         });
  //       }
  //     });
  // };

  const addToCartFunction = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/carts/add-to-cart`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({ productId: productId, quantity: quantity }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          // Swal.fire({
          //   title: "Added to cart",
          //   icon: "success",
          //   text: "You have added a product to cart",
          // });
          Swal.fire({
            title: "Added to cart!",
            icon: "success",
            text: "You have added a product to cart",
            timer: 1500,
            timerProgressBar: true,
            didOpen: () => {
              Swal.showLoading();
            },
          }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
              console.log("I was closed by the timer");
            }
          });
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please, try again.",
          });
        }
      });
  };

  const routeToCheckOutPage = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/carts/add-to-cart`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({ productId: productId, quantity: quantity }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          // Swal.fire({
          //   title: "Added to cart",
          //   icon: "success",
          //   text: "You have added a product to cart",
          // });
          navigate("/checkout");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please, try again.",
          });
        }
      });
  };

  useEffect(() => {
    if (quantity !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [quantity]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProducts(
          <Row className="mt-3 mb-3">
            <Col md={6} lg={4} className="mx-auto my-2">
              <Card className="cardHighlight p-0">
                <Card.Body>
                  <Card.Title>
                    <h4 className="text-center">{data.name}</h4>
                  </Card.Title>
                  <img src={data.imgLink} class="card-img-top p-3" alt="Card" />
                  <Card.Subtitle>Product ID</Card.Subtitle>
                  <Card.Text>{data._id}</Card.Text>
                  <Card.Subtitle>Name</Card.Subtitle>
                  <Card.Text>{data.name}</Card.Text>
                  <Card.Subtitle>Description</Card.Subtitle>
                  <Card.Text>{data.description}</Card.Text>
                  <Card.Subtitle>Price</Card.Subtitle>
                  <Card.Text>{data.price}</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        );
      });
  }, [productId]);

  return (
    <>
      {products}
      <Row className="mt-3 mb-3 text-center">
        <Col md={6} lg={4} className="mx-auto my-2">
          <Button className="bg-primary" onClick={() => navigate(-1)}>
            Go back to orders
          </Button>
        </Col>
      </Row>
    </>
  );
}
