import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Button, Row, Col } from "react-bootstrap";

import OrderCardAdmin from "../components/OrderCardAdmin";

export default function AdminGetActiveOrders() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/all-orders`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setOrders(
          data.map((order) => {
            return (
              <>
                {" "}
                <Col md={6} lg={4} className="mx-auto my-2">
                  <OrderCardAdmin key={order._id} order={order} />
                </Col>
              </>
            );
          })
        );
      });
  }, []);

  return (
    <>
      <Button className="bg-primary mt-2" as={Link} to={`/orders`}>
        Table View
      </Button>
      <Row>{orders}</Row>
    </>
  );
}
