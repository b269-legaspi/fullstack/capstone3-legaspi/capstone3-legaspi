import { useState, useEffect } from "react";

import { Form, Button } from "react-bootstrap";

import Swal from "sweetalert2";

import { useParams, useNavigate } from "react-router-dom";

export default function UpdateProduct({ product }) {
  const navigate = useNavigate();

  const { productId } = useParams();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [imgLink, setImgLink] = useState("");
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setImgLink(data.imgLink);
      });
  }, [productId]);

  const updateProduct = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
        imgLink: imgLink,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "Successfully updated",
            icon: "success",
            text: "You have successfully updated this product.",
          });

          navigate("/admin-dashboard");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  };

  useEffect(() => {
    if (name !== "" || description !== "" || price !== "" || imgLink !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [name, description, price, imgLink]);

  return (
    <Form onSubmit={(e) => updateProduct(e)}>
      <Form.Group controlId="name">
        <Form.Label>Product Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Product Name Here"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="description">
        <Form.Label>Description</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Description Here"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="price">
        <Form.Label>Price</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter Price Here"
          value={price}
          onChange={(e) => setPrice(e.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="imgLink">
        <Form.Label>Image Link</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Image Link Here"
          value={imgLink}
          onChange={(e) => setImgLink(e.target.value)}
        />
      </Form.Group>

      {isActive ? (
        <Button className="my-2" variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button
          className="my-2"
          variant="danger"
          type="submit"
          id="submitBtn"
          disabled
        >
          Submit
        </Button>
      )}
    </Form>
  );
}
