import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Button, Row, Col, ListGroup } from "react-bootstrap";

export default function UserDetails() {
  const [userInfo, setUserInfo] = useState("");
  const [userTotalExpenses, setUserTotalExpenses] = useState("");
  const [userOrderedProducts, setUserOrderedProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUserInfo(
          <Col md={6} lg={4} className="text-center mx-auto mt-2">
            <ListGroup variant="flush">
              <ListGroup.Item>
                <h4 className="text-center">{data.email}</h4>
              </ListGroup.Item>
              <ListGroup.Item>User ID: {data._id}</ListGroup.Item>
              <ListGroup.Item></ListGroup.Item>
            </ListGroup>
            <Button variant="primary" as={Link} to={"/change-pass"}>
              Change password
            </Button>
          </Col>
        );
      });
  }, []);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/user-total-expense`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUserTotalExpenses(
          <Col md={6} lg={4} className="text-center mx-auto">
            <ListGroup variant="flush">
              <ListGroup.Item></ListGroup.Item>
              <ListGroup.Item>{data.value}</ListGroup.Item>
              <ListGroup.Item></ListGroup.Item>
            </ListGroup>
          </Col>
        );
      });
  }, []);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/user-quantity-ordered`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUserOrderedProducts(
          data.length === 0 ? (
            <Col md={6} lg={4} className="text-center mx-auto mt-4">
              <h7 className="text-center">0 products ordered</h7>
            </Col>
          ) : (
            data.map((products) => {
              return (
                <Col md={6} lg={4} className="mx-auto my-2">
                  <ListGroup variant="flush">
                    <ListGroup.Item></ListGroup.Item>
                    <ListGroup.Item>
                      Product Name: {products._id}
                    </ListGroup.Item>
                    <ListGroup.Item>Quantity: {products.total}</ListGroup.Item>
                    <ListGroup.Item></ListGroup.Item>
                  </ListGroup>
                </Col>
              );
            })
          )
        );
      });
  }, []);

  return (
    <>
      <Row>{userInfo}</Row>
      <Row>{userTotalExpenses}</Row>
      <Row>
        <Col md={6} lg={4} className="text-center mx-auto mt-4">
          <h6 className="text-center">Total Products Ordered</h6>
        </Col>
      </Row>
      <Row>{userOrderedProducts}</Row>
    </>
  );
}
