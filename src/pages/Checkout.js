import { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";

import Banner from "../components/Banner";
import CartCard from "../components/CartCard";
import CheckoutCard from "../components/CheckoutCard";

export default function Checkout() {
  const [cartProducts, setCartProducts] = useState([]);
  const [cartTotal, setCartTotal] = useState([]);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/carts/user-cart`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setLoading(false); //hide Loader.js when data is fetched
        // console.log(data);
        // console.log(data[0].cartProducts);
        if (data[0].cartProducts.length !== 0) {
          setCartTotal(<CheckoutCard key={data[0]._id} data={data[0]} />);
        }
        const emptyCartData = {
          title: "No products to checkout",
          content: "Add products to cart",
          destination: "/products",
          label: "View our Products",
        };
        setCartProducts(
          data[0].cartProducts.length === 0 ? (
            <Banner data={emptyCartData} />
          ) : (
            data[0].cartProducts.map((cart) => {
              return (
                <Col md={6} lg={4} className="mx-auto my-2">
                  <CartCard
                    key={cart.productId}
                    isLoading={isLoading}
                    cart={cart}
                  />
                </Col>
              );
            })
          )
        );
      })
      .catch(() => {
        const noExistingCartData = {
          title: "No products to checkout",
          content: "Add products to cart",
          destination: "/products",
          label: "View our Products",
        };
        setCartProducts(
          <>
            <Banner data={noExistingCartData} />
          </>
        );
      });
  }, [isLoading]);

  return (
    <>
      <Row>{cartProducts}</Row>
      {cartTotal}
    </>
  );
}
