import Banner from "../components/Banner";
import Login from "../pages/Login";

import { Link } from "react-router-dom";

import { Button } from "react-bootstrap";

export default function Home() {
  const data = {
    title: "Order your food here!",
    content: "Affordable meals for you and your loved ones",
    destination: "/products",
    label: "See our Products",
  };

  return (
    <>
      <Banner data={data} />
      <h4>Login to Order our Products</h4>
      <Login />
      <h4 className="my-3">Not yet registered?</h4>
      <Button variant="primary" as={Link} to="/register">
        Register Here
      </Button>
    </>
  );
}
