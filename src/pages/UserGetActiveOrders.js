import { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";

import Banner from "../components/Banner";
import OrderCardUser from "../components/OrderCardUser";

export default function UserGetActiveOrders() {
  const [orders, setOrders] = useState([]);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/user-orders`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setLoading(false); //hide Loader.js when data is fetched
        const emptyOrdersData = {
          title: "No existing orders to show",
          content: "Order your food now! For you and your loved ones.",
          destination: "/products",
          label: "View our Products",
        };
        setOrders(
          data.length === 0 ? (
            <Banner data={emptyOrdersData} />
          ) : (
            data.map((order) => {
              console.log(order._id); // this is a unique key..
              return (
                <Col md={6} lg={4} className="mx-auto my-2">
                  <OrderCardUser
                    key={order._id}
                    isLoading={isLoading}
                    order={order}
                  />
                </Col>
              );
            })
          )
        );
      });
  }, [isLoading]);

  return (
    <>
      <Row>{orders}</Row>
    </>
  );
}
