import { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";

import Swal from "sweetalert2";

import { useNavigate } from "react-router-dom";

export default function CreateProduct() {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [imgLink, setImgLink] = useState("");
  const [isActive, setIsActive] = useState(false);

  const navigate = useNavigate();

  function createProduct(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
        imgLink: imgLink,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "New Product Created!",
            icon: "success",
            text: `${name}`,
          });

          navigate("/admin-dashboard");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please, try again.",
          });
        }
      });
  }

  useEffect(() => {
    if (name !== "" && description !== "" && price !== "" && imgLink !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [name, description, price, imgLink]);

  return (
    <Form onSubmit={(e) => createProduct(e)}>
      <Form.Group controlId="name">
        <Form.Label>Product Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Product Name Here"
          value={name}
          onChange={(e) => setName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="description">
        <Form.Label>Description</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Description Here"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="price">
        <Form.Label>Price</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter Price Here"
          value={price}
          onChange={(e) => setPrice(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="imgLink">
        <Form.Label>Image Link</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Image Link Here"
          value={imgLink}
          onChange={(e) => setImgLink(e.target.value)}
          required
        />
      </Form.Group>

      {isActive ? (
        <Button className="my-2" variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button
          className="my-2"
          variant="danger"
          type="submit"
          id="submitBtn"
          disabled
        >
          Submit
        </Button>
      )}
    </Form>
  );
}
