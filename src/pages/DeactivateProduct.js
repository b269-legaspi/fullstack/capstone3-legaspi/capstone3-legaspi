import { useState, useEffect } from "react";

import { Form, Button } from "react-bootstrap";

import Swal from "sweetalert2";

import { useParams, useNavigate } from "react-router-dom";

export default function DeactivateProduct({ product }) {
  const navigate = useNavigate();
  const { productId } = useParams();

  const [productIsActive, setProductIsActive] = useState("");
  const [isActive, setIsActive] = useState(false);

  const updateProduct = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        isActive: productIsActive,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "Successfully updated",
            icon: "success",
            text: "You have successfully updated this product.",
          });

          navigate("/products");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  };

  useEffect(() => {
    if (productIsActive !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [productIsActive]);

  return (
    <Form onSubmit={(e) => updateProduct(e)}>
      <Form.Group controlId="productIsActive">
        <Form.Label>Is active?</Form.Label>
        <Form.Control
          type="text"
          placeholder="True or false"
          value={productIsActive}
          onChange={(e) => setProductIsActive(e.target.value)}
        />
      </Form.Group>

      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
