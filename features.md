(minimum features)
● Register Page
● Login Page
● Products Catalog Page
○○○ Retrieve all active products
○○○ Retrieve single product
● Admin Dashboard
○○○ Create Product
○○○ Retrieve all products
○○○ Update Product information
○○○ Deactivate/reactivate product
● Checkout Order
○○○ Non-admin User checkout (Create Order)

(stretch goal)
● Full responsiveness across mobile/tablet/desktop screen sizes
● Product images
● View User Details (Profile) - You can change their password
● Setting a user as an admin
● Add to Cart Feature
○○○ Show all items the user has added to their cart (and their quantities)
○○○ Change product quantities
○○○ Remove products from cart
○○○ Subtotal for each item
○○○ Total price for all items
○○○ A working checkout button/functionality.
○○○ When the user checks their cart out, redirect them to the Order History page
● Order History
○○○ Retrieve a list of all orders made by user
○○○ Admin feature to retrieve a list of all orders made by all users

(personal stretch goals)
● Order History
○○○ table view and card view available,
○○○ Admin feature to retrieve a list of all orders for a specific user

link:
https://e-commerce-app-legaspi.vercel.app/
